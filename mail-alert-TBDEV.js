// ==UserScript==
// @name           Message Poller
// @namespace      MessagePoller
// @homepageURL    http://www.progressivethink.in/wp-content/uploads/userscripts/messagePoller/messagePoller.user.js
// @description    Polls for new messages
// @svc:version    [1.0.7]
// @version        1.0.7

// @history        (1.0.7) Moved to generic formats
// @history        (1.0.6) Misc. Fixes (Try/Catch)
// @history        (1.0.5) Misc. Fixes (FB)
// @history        (1.0.4) Misc. Fixes
// @history        (1.0.3) Misc. Fixes
// @history        (1.0.2) Added Sound
// @history        (1.0.1) Initial Commit

// @updateURL      http://www.progressivethink.in/wp-content/uploads/userscripts/messagePoller/messagePoller.user.js

// @include        http://*/chat.php

// @copyright  2012+, Boondoklife
// ==/UserScript==

/*
Copyright 2011 Boondoklife

This file is part of TorrentDay Message Poller.

TorrentDay Message Poller is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TorrentDay Chat Mod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TorrentDay Chat Mod.  If not, see <http://www.gnu.org/licenses/>.
*/
try {

var seconds = 15;
var poller;
var mailCount = 0;

// ######################################################################
// # Append Audio Object
// ######################################################################
var voz = "http://www.progressivethink.in/wp-content/uploads/userscripts/messagePoller/mail.ogg";
var alertAudioObject = document.createElement('audio');
    alertAudioObject.setAttribute("src", contra(voz));
    alertAudioObject.setAttribute("id", "alertAudioObject");
	
function getMessages()
{
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            var regMatch = /.*>You have (\d+) new message[s]?!<\.*/gi;
            var result = regMatch.exec(xmlhttp.responseText);
			if (result != null)
			{
				result = result[1];
			}
			else
			{
				return;
			}
           
			console.log(result+"|"+mailCount);
            if (result != mailCount)
            {
				if (result > mailCount)
				{
					alertAudioObject.play();
				}
				
				mailCount = result;
			}
			
        }
    }

    xmlhttp.open("GET","inbox.php",true);
    xmlhttp.send();
}

poller = self.setInterval(getMessages, seconds*1000);
getMessages();
}
catch(err)
{}
